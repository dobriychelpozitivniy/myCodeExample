package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"mailer-auth/pkg/handler"
	"mailer-auth/pkg/models"
	"mailer-auth/pkg/repository"
	"mailer-auth/pkg/service"
	"time"
)

type Config struct {
	Host            string `mapstructure:"host"`
	Port            int    `mapstructure:"port"`
	Key             string `mapstructure:"key"`
	AccessTokenTtl  int    `mapstructure:"access_token_ttl"`
	RefreshTokenTtl int    `mapstructure:"refresh_token_ttl"`
	DBHost          string `mapstructure:"db_host"`
	DBUsername      string `mapstructure:"db_username"`
	DBPassword      string `mapstructure:"db_password"`
	DBPort          string `mapstructure:"db_port"`
	DBTimeout       int `mapstructure:"db_timeout"`
	DBName string  `mapstructure:"db_name"`
}

func main() {
	config, err := InitConfig()
	if err != nil {
		logrus.Fatalf("Error init config: %s", err.Error())
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		ForceColors:   true,
	})
	logrus.Infof("host: %s\n", config.Host)
	logrus.Infof("port: %d\n", config.Port)
	logrus.Infof("key: %s\n", config.Key)
	logrus.Infof("access_token_ttl: %d\n", config.AccessTokenTtl)
	logrus.Infof("refresh_token_ttl: %d\n", config.RefreshTokenTtl)

	db, err := ConnectMongo(*config)
	if err != nil {
		logrus.Fatalf("Error connect mongodb: %s", err.Error())
	}

	logrus.Infoln("Mongodb has been connected")


	repos := repository.NewRepository(db)
	services := service.NewService(config.Key, repos)
	handlers := handler.NewHandler(handler.Config{
		Host:            config.Host,
		AccessTokenTtl:  config.AccessTokenTtl,
		RefreshTokenTtl: config.RefreshTokenTtl,
		Key:             config.Key,
	}, services, false)


	if !checkExistTestUser(db) {
		pswr, err := services.Hash("wwww")
		_, err = db.Collection(viper.GetString("db_collection")).InsertOne(context.TODO(), models.User{
			Username:     "testuser",
			PasswordHash: pswr,
		})
		if err != nil {
			logrus.Fatalf("Error create testuser in db: %s", err)
		}
	}


	g := InitServer(handlers)
	if err := g.Run(fmt.Sprintf("%s:%d", config.Host, config.Port)); err != nil {
		logrus.Fatalf("Error start server: %s", err.Error())
	}

}

func InitConfig() (*Config, error) {
	var config Config

	viper.AddConfigPath("configs/dev")
	viper.AddConfigPath("/chupapimunyanya/configs") // for docker
	viper.SetConfigName("config")
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	return &config, nil
}

func InitServer(routes *handler.Handler) *gin.Engine {
	g := gin.New()
	g.Use(gin.Logger())
	routes.InitRoutes(g)
	return g
}

func ConnectMongo(c Config) (*mongo.Database, error) {
	connPattern := "mongodb://%v:%v@%v:%v"
	if c.DBUsername == "" {
		connPattern = "mongodb://%s%s%v:%v"
	}

	clientUrl := fmt.Sprintf(connPattern,
		c.DBUsername,
		c.DBPassword,
		c.DBHost,
		c.DBPort,
	)
	clientOptions := options.Client().ApplyURI(clientUrl)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		return nil, err
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Duration(c.DBTimeout)*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return client.Database(c.DBName), nil
}

func checkExistTestUser(db *mongo.Database) bool {
	filter := bson.D{
		{"username", bson.M{"$eq": "testuser"}},
	}

	res := db.Collection(viper.GetString("db_collection")).FindOne(context.TODO(), filter)


	var user *models.User
	err := res.Decode(&user)
	if err != nil {
		return false
	}


	return true
}
