FROM registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine as builder

WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -o auth ./cmd/main.go


# generate clean, final image for end users
FROM quay.io/jitesoft/alpine:3.11.3

COPY --from=builder /build/auth /chupapimunyanya/
COPY --from=builder /build/configs/prod /chupapimunyanya/configs/
COPY --from=builder /build/db /chupapimunyanya/db/

# executable
ENTRYPOINT [ "/chupapimunyanya/auth" ]
