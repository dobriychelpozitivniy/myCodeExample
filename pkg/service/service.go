package service

import (
	"mailer-auth/pkg/models"
	"mailer-auth/pkg/repository"
	"time"
)

//go:generate mockgen -source=service.go -destination=mocks/mock.go

type PasswordManager interface {
	Hash(password string) (string, error)
	Check(password string, hash string) (bool, error)
}

type User interface {
	GetUser(username string) (*models.User, error)
}

type TokenManager interface {
	GenerateRefreshToken(userId string, ttl time.Duration) (string, error)
	GenerateAccessToken(userId string, ttl time.Duration) (string, error)
	ParseAccessToken(AccessTokenClaims string) (*models.AccessTokenClaims, error)
	ParseRefreshToken(RefreshToken string) (*models.RefreshTokenClaims, error)
}

type Service struct {
	PasswordManager
	TokenManager
	User
}

func NewService(signingKey string, repos *repository.Repository) *Service {
	return &Service{
		PasswordManager: NewPasswordManagerService(),
		TokenManager: NewTokenManagerService(signingKey),
		User: NewUserService(repos.Users),
	}
}