package service

import (
	"golang.org/x/crypto/bcrypt"
)

type PasswordManagerService struct{
}

func NewPasswordManagerService() *PasswordManagerService {
	return &PasswordManagerService{}
}

func (m *PasswordManagerService) Hash(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

func (m *PasswordManagerService) Check(password string, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}
