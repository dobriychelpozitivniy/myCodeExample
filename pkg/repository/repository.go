package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"mailer-auth/pkg/models"
)

type Users interface {
	Read(ctx context.Context,username string) (*models.User, error)
}

type Repository struct {
	Users
}

func NewRepository(db *mongo.Database) *Repository {
	return &Repository{
		Users: NewUsersRepository(db),
	}
}