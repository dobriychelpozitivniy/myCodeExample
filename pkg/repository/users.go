package repository

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"mailer-auth/pkg/models"
)

type UsersRepository struct {
	db *mongo.Database
}

func NewUsersRepository(db *mongo.Database) *UsersRepository {
	return &UsersRepository{db: db}
}

func (r *UsersRepository) Read(ctx context.Context,username string) (*models.User, error) {
	filter := bson.D{
		{"username", bson.M{"$eq": username}},
	}

	res := r.db.Collection(viper.GetString("db_collection")).FindOne(ctx, filter)


	var user *models.User
	err := res.Decode(&user)
	logrus.Infoln("USER: ",user)
	if err != nil {
		logrus.Infoln("DECODE ERORR", err.Error())
		return nil, err
	}

	logrus.Infoln("USER: ",user)

	return user, nil
}
