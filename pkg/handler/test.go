package handler

import (
	"github.com/spf13/viper"
)

func InitConfig() (Config, error) {
	var config Config

	viper.AddConfigPath("../../configs/dev")
	viper.AddConfigPath("/chupapimunyanya/configs") // for docker
	viper.SetConfigName("config")
	if err := viper.ReadInConfig(); err != nil {
		return config, err
	}

	if err := viper.Unmarshal(&config); err != nil {
		return config, err
	}

	return config, nil
}

