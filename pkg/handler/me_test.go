package handler

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"mailer-auth/pkg/models"
	"mailer-auth/pkg/service"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestHandler_me(t *testing.T) {
	testCases := []struct{
		Name string
		ExpectedCode int
		ExpectedValue interface{}
		ContextValue models.UserClaims
		ContextKey string
		BodyToken string
	}{
		{
			Name:          "valid key and value",
			ExpectedCode:  200,
			ExpectedValue: "testuser",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "User",
			BodyToken: "",
		},
		{
			Name:          "invalid key",
			ExpectedCode:  500,
			ExpectedValue: "",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "invalid",
			BodyToken: "none",
		},
		{
			Name:          "invalid value",
			ExpectedCode:  500,
			ExpectedValue: "",
			ContextValue:  models.UserClaims{},
			ContextKey:    "User",
			BodyToken: "none",
		},
		{
			Name:          "valid access token",
			ExpectedCode:  200,
			ExpectedValue: "testuser",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "User",
			BodyToken:     "",
		},
		{
			Name:          "invalid access token",
			ExpectedCode:  400,
			ExpectedValue: "",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "User",
			BodyToken:     "wqjdoqwhdiquwfhaosldaiwodj",
		},
	}

	cfg, err := InitConfig()
	if err != nil {
		t.Errorf("Error init config: %s", err)
	}

	tm := service.NewTokenManagerService("asdf")

	services := &service.Service{
		TokenManager:    tm,
	}

	handler := NewHandler(cfg, services, false)

	for _, tC := range testCases {
		t.Run(tC.Name, func(t *testing.T) {
			if tC.BodyToken == "" {
				tC.BodyToken, _ = handler.services.GenerateAccessToken("testuser", 60)
			}

			r := gin.New()

			r.Use(func(c *gin.Context) {
				c.Set(tC.ContextKey, tC.ContextValue)
			})

			r.POST("/me", handler.me)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)

			body, _ := json.Marshal(models.BodyRequest{AccessToken: tC.BodyToken})

			c.Request, _ = http.NewRequest("POST", "/me", strings.NewReader(string(body)))

			r.ServeHTTP(w, c.Request)

			var user models.UserClaims

			logrus.Infoln(w.Body)
			logrus.Infoln(w.Code)
			body, err := ioutil.ReadAll(w.Body)
			if err != nil {
				t.Error(err.Error())
			}

			err = json.Unmarshal(body, &user)
			if err != nil {
				t.Error(err.Error())
			}

			assert.Equal(t, tC.ExpectedValue, user.UserId)
			assert.Equal(t, tC.ExpectedCode, w.Code)
		})
	}


}
