package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"mailer-auth/pkg/models"
	"net/http"
	"time"
)

func (h *Handler) isAuthorized() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken, ok := getAccessCookieToken(c)
		if !ok {
			refreshToken, ok := getRefreshCookieToken(c)
			if !ok {
				logrus.WithFields(logrus.Fields{
					"method": "isAuthorized",
					"code":   http.StatusUnauthorized,
					"time": fmt.Sprintf("[%s]", time.Now().String()),
				}).Infoln("empty cookies")
				c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
					Code: http.StatusUnauthorized,
					Message: "empty cookies",
				})
				return
			}

			refreshClaims, err := h.services.ParseRefreshToken(refreshToken)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"method": "isAuthorized",
					"code":   http.StatusUnauthorized,
					"time": fmt.Sprintf("[%s]", time.Now().String()),
				}).Infoln("invalid refresh-token")
				c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
					Code:    http.StatusUnauthorized,
					Message: "invalid refresh-token",
				})
				return
			}

			accessToken, _ := setNewTokensInCookie(c, h, refreshClaims.UserId)

			accessClaims, err := h.services.ParseAccessToken(accessToken)

			c.Set("User", accessClaims.User)
			c.Next()

		} else {
			accessClaims, err := h.services.ParseAccessToken(accessToken)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"method": "isAuthorized",
					"code":   http.StatusUnauthorized,
					"time": fmt.Sprintf("[%s]", time.Now().String()),
				}).Infoln("invalid access-token")
				c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
					Code:    http.StatusUnauthorized,
					Message: "invalid access-token",
				})
				return
			}

			setNewTokensInCookie(c, h, accessClaims.User.UserId)

			c.Set("User", accessClaims.User)
			c.Next()
		}
	}
}

func setNewTokensInCookie(c *gin.Context,h *Handler, userId string) (string, string) {
	newAccessToken, err := h.services.GenerateAccessToken(userId, time.Duration(h.config.AccessTokenTtl))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Cant generate access-token",
		})
		logrus.WithFields(logrus.Fields{
			"method": "isAuthorized",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return "", ""
	}

	newRefreshToken, err := h.services.GenerateRefreshToken(userId, time.Duration(h.config.RefreshTokenTtl))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Cant generate refresh-token",
		})
		logrus.WithFields(logrus.Fields{
			"method": "isAuthorized",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return "", ""
	}

	c.SetCookie(
		"refresh-token",
		newRefreshToken,
		h.config.RefreshTokenTtl,
		"/",
		h.config.Host,
		false,
		true,
	)
	c.SetCookie(
		"access-token",
		newAccessToken,
		h.config.AccessTokenTtl,
		"/",
		h.config.Host,
		false,
		true,
	)

	return newAccessToken, newRefreshToken
}

func getAccessCookieToken(c *gin.Context) (string, bool) {
	token, err := c.Cookie("access-token")
	if err != nil {
			return "", false
	}

	return token, true
}

func getRefreshCookieToken(c *gin.Context) (string, bool) {
	token, err := c.Cookie("refresh-token")
	if err != nil {
		return "", false
	}

	return token, true
}
