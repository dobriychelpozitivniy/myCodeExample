package handler

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"mailer-auth/pkg/models"
	"mailer-auth/pkg/service"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_I(t *testing.T) {
	testCases := []struct{
		Name string
		ExpectedCode int
		ExpectedValue interface{}
		ContextValue models.UserClaims
		ContextKey string
		ExpectedMessage string
	}{
		{
			Name:          "valid key and value",
			ExpectedCode:  200,
			ExpectedValue: "testuser",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "User",
		},
		{
			Name:          "invalid key",
			ExpectedCode:  500,
			ExpectedValue: "",
			ContextValue:  models.UserClaims{UserId: "testuser"},
			ContextKey:    "invalid",
		},
		{
			Name:          "invalid value",
			ExpectedCode:  500,
			ExpectedValue: "",
			ContextValue:  models.UserClaims{},
			ContextKey:    "User",
		},

	}

	cfg, err := InitConfig()
	if err != nil {
		t.Errorf("Error init config: %s", err)
	}

	services := &service.Service{}
	handler := NewHandler(cfg, services, false)

	for _, tC := range testCases {
		t.Run(tC.Name, func(t *testing.T) {
			r := gin.New()

			r.Use(func(c *gin.Context) {
				c.Set(tC.ContextKey, tC.ContextValue)
			})

			r.POST("/i", handler.i)

			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)

			c.Request, _ = http.NewRequest("POST", "/i", nil)

			// Make Request
			r.ServeHTTP(w, c.Request)

			var user models.UserClaims

			logrus.Infoln(w.Body)
			logrus.Infoln(w.Code)
			body, err := ioutil.ReadAll(w.Body)
			if err != nil {
				t.Error(err.Error())
			}

			err = json.Unmarshal(body, &user)
			if err != nil {
				t.Error(err.Error())
			}

			assert.Equal(t, tC.ExpectedValue, user.UserId)
			assert.Equal(t, tC.ExpectedCode, w.Code)
		})
	}
}
