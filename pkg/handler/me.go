package handler

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"mailer-auth/pkg/models"
	"net/http"
	"reflect"
	"time"
)

func (h *Handler) me(c *gin.Context) {
	user, ok := c.Get("User")
	if !ok {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Some problems in server",
		})
		logrus.WithFields(logrus.Fields{
			"method": "me",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return
	}

	v := reflect.ValueOf(user)

	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Interface() == "" {
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Code:    http.StatusInternalServerError,
				Message: "Some problems in server",
			})
			logrus.WithFields(logrus.Fields{
				"method": "i",
				"code":   http.StatusInternalServerError,
				"time": fmt.Sprintf("[%s]", time.Now().String()),
			}).Info("")
			return
		}
	}


	if c.GetHeader("X-SERVICE-NAME") == "profile/i" {
		user = user.(models.UserClaims)

		logrus.WithFields(logrus.Fields{
			"method": "me",
			"code":   http.StatusOK,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Infof("user: %v send request from profile service", user)
		c.JSON(200, user)
		return
	}

	jsonData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Some problems in server",
		})
		logrus.WithFields(logrus.Fields{
			"method": "me",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return
	}

	var bodyRequest models.BodyRequest
	if err := json.Unmarshal(jsonData, &bodyRequest); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, &models.Response{
			Code:    http.StatusBadRequest,
			Message: "Bad request",
		})
		return
	}

	userFromBody, err := h.services.ParseAccessToken(bodyRequest.AccessToken)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, &models.Response{
			Code:    http.StatusBadRequest,
			Message: "Invalid access-token in body",
		})
		return
	}

	logrus.WithFields(logrus.Fields{
		"method": "me",
		"code":   http.StatusOK,
		"time": fmt.Sprintf("[%s]", time.Now().String()),
	}).Infof("user: %v send request", userFromBody.User)
	c.JSON(200, userFromBody.User)
}