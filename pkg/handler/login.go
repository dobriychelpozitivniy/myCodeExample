package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"mailer-auth/pkg/models"
	"net/http"
	"time"
)

func (h *Handler) login(c *gin.Context) {
	username, password, ok := c.Request.BasicAuth()
	if !ok {
		c.AbortWithStatusJSON(http.StatusBadRequest, &models.Response{
			Code:    http.StatusBadRequest,
			Message: "Wrong authorization header or value",
		})
		return
	}

	user, err := h.services.User.GetUser(username)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
			Code:    http.StatusUnauthorized,
			Message: "Cant get user on login",
		})
		return
	}

	valid, err := h.services.PasswordManager.Check(password, user.PasswordHash)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
			Code:    http.StatusUnauthorized,
			Message: "Wrong password",
		})
		return
	}
	if !valid {
		c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
			Code:    http.StatusUnauthorized,
			Message: "Wrong password",
		})
		return
	}

	logrus.Infoln("LOG ID: ", user.Username)
	refreshToken, err := h.services.TokenManager.GenerateRefreshToken(user.Username, time.Duration(h.config.RefreshTokenTtl))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Cant generate refresh-token",
		})
		logrus.WithFields(logrus.Fields{
			"method": "login",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return
	}
	accessToken, err := h.services.TokenManager.GenerateAccessToken(user.Username, time.Duration(h.config.AccessTokenTtl))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Cant generate access-token",
		})
		logrus.WithFields(logrus.Fields{
			"method": "login",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Warn("")
		return
	}
	c.SetCookie(
		"refresh-token",
		refreshToken,
		h.config.RefreshTokenTtl,
		"/",
		h.config.Host,
		false,
		true,
	)
	c.SetCookie(
		"access-token",
		accessToken,
		h.config.AccessTokenTtl,
		"/",
		h.config.Host,
		false,
		true,
	)

	redirect := c.Query("redirect_uri")
	if redirect != "" {
		logrus.WithFields(logrus.Fields{
			"method": "login",
			"code":   http.StatusPermanentRedirect,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Infof("user: %v redirect to %s", user, redirect)
		c.Redirect(http.StatusPermanentRedirect, redirect)
		return
	}

	logrus.WithFields(logrus.Fields{
		"method": "login",
		"code":   http.StatusOK,
		"time": fmt.Sprintf("[%s]", time.Now().String()),
	}).Infof("user: %v is login", user)
	c.JSON(http.StatusOK, &models.Response{
		Code:    http.StatusOK,
		Message: "Successful login",
	})
}