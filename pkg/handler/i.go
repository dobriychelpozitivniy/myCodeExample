package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"mailer-auth/pkg/models"
	"net/http"
	"reflect"
	"time"
)

func (h *Handler) i(c *gin.Context) {
	user, ok := c.Get("User")
	if !ok {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
			Code:    http.StatusInternalServerError,
			Message: "Some problems in server",
		})
		logrus.WithFields(logrus.Fields{
			"method": "i",
			"code":   http.StatusInternalServerError,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Info("")
		return
	}

	v := reflect.ValueOf(user)

	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Interface() == "" {
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Code:    http.StatusInternalServerError,
				Message: "Some problems in server",
			})
			logrus.WithFields(logrus.Fields{
				"method": "i",
				"code":   http.StatusInternalServerError,
				"time": fmt.Sprintf("[%s]", time.Now().String()),
			}).Info("")
			return
		}
	}

	user = user.(models.UserClaims)
	logrus.WithFields(logrus.Fields{
		"method": "i",
		"code":   http.StatusOK,
		"time": fmt.Sprintf("[%s]", time.Now().String()),
	}).Infof("user: %v send request", user)
	c.JSON(200, user)
}
