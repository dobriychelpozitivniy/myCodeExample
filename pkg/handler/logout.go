package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"mailer-auth/pkg/models"
	"net/http"
	"time"
)

func(h *Handler) logout(c *gin.Context) {
	c.SetCookie(
		"refresh-token",
		"",
		-1,
		"/",
		h.config.Host,
		false,
		true,
	)
	c.SetCookie(
		"access-token",
		"",
		-1,
		"/",
		h.config.Host,
		false,
		true,
	)

	redirect := c.Query("redirect_uri")
	if redirect != "" {
		logrus.WithFields(logrus.Fields{
			"method": "logout",
			"code":   http.StatusPermanentRedirect,
			"time": fmt.Sprintf("[%s]", time.Now().String()),
		}).Infof("redirect to %s", redirect)
		c.Redirect(http.StatusPermanentRedirect, redirect)
		return
	}

	logrus.WithFields(logrus.Fields{
		"method": "logout",
		"code":   http.StatusOK,
		"time": fmt.Sprintf("[%s]", time.Now().String()),
	}).Info("")
	c.AbortWithStatusJSON(http.StatusOK, &models.Response{
		Code:    http.StatusOK,
		Message: "Successful logout",
	})
}