package models

import "github.com/golang-jwt/jwt"

type UserClaims struct {
	UserId string `json:"user_id" binding:"required"`
}

type AccessTokenClaims struct {
	User UserClaims
	jwt.StandardClaims
}

type RefreshTokenClaims struct {
	UserId string `json:"user_id" binding:"required"`
	jwt.StandardClaims
}
